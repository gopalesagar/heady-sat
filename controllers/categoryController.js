let Validator = require('validatorjs');
const CategorySchema = require('../models/category');

exports.create = (req, res) => {
    const name = req.body.name;
    const products = req.body.products;
    const childCategories = req.body.childCategories;

    let validation = new Validator({
        name: name
    }, {
        name: 'required'
    })

    if (validation.fails()) {
        res.status(412).send(validation.errors.errors)
    } else {
        let childCategoryArray = [];

        if (childCategories) {
            childCategories.forEach(cc => {
                let tempCc = {
                    name: cc.name,
                    childCategories: cc.childCategories
                }
                childCategoryArray.push(tempCc);
            });
        }

        let Category = new CategorySchema({
            name: name,
            products: products,
            childCategories: childCategoryArray
        })

        Category.save(function(error, result) {
            if(error) {
                res.status(error.statusCode ? error.statusCode : 500).send(error.message ? error.message : error);
            } else {
                res.status(200).send(result);
            }
        })
    }
}

exports.list = (req, res) => {
    const skip = req.query.skip ? parseInt(req.query.skip) : null;
    const limit = req.query.limit ? parseInt(req.query.limit) : null;
    CategorySchema.find()
    .select({
        products: 1,
        name: 1,
        child_categories: 1
    })
    .skip(skip)
    .limit(limit)
    .sort({
        name: 'asc'
    })
    .exec((error, result) => {
        if(error) {
            res.status(error.statusCode ? error.statusCode : 500).send(error.message ? error.message : error);
        } else {
            res.status(200).send(result);
        }
    })
}