let Validator = require('validatorjs');
const ProductSchema = require('../models/product');

exports.create = (req, res) => {

    const name = req.body.name;
    const pricePerUnit = req.body.pricePerUnit;
    const priceUnit = req.body.priceUnit;
    const unit = req.body.unit;
    const categories = req.body.categories;

    let validation = new Validator({
        name: name,
        pricePerUnit: pricePerUnit,
        priceUnit: priceUnit,
        categories: categories
    }, {
        name: 'required',
        pricePerUnit: 'required|numeric',
        priceUnit: 'required',
        categories: 'required'
    })

    if (validation.fails()) {
        res.status(412).send(validation.errors.errors)
    } else {

        let Product = new ProductSchema({
            name: name,
            pricePerUnit: pricePerUnit,
            priceUnit: priceUnit,
            unit: unit,
            categories: categories
        })

        Product.save(function(error, result) {
            if(error) {
                res.status(error.statusCode ? error.statusCode : 500).send(error.message ? error.message : error);
            } else {
                res.status(200).send(result);
            }
        })
    }
}

exports.find = (req, res) => {
    const skip = req.query.skip ? parseInt(req.query.skip) : null;
    const limit = req.query.limit ? parseInt(req.query.limit) : null;
    const category = req.query.category;
    
    ProductSchema.find()
    .populate({
        path: 'categories',
        match: {
            name: category
        }
    })
    .skip(skip)
    .limit(limit)
    .exec((error, result) => {
        if(error) {
            res.status(error.statusCode ? error.statusCode : 500).send(error.message ? error.message : error);
        } else {
            res.status(200).send(result);
        }
    })
}

exports.update = (req, res) => {
    const productId = req.body.productId;
    const name = req.body.name;
    const price = req.body.price;
    const categoriesToAdd = req.body.categoriesToAdd

    let validation = new Validator({
        productId: productId
    }, {
        productId: 'required'
    })

    if (validation.fails()) {
        res.status(412).send(validation.errors.errors)
    } else {
        ProductSchema.findOneAndUpdate({
            _id: productId
        }, {
            name: name,
            pricePerUnit: price,
            $addToSet: {
                categories: { $each: categoriesToAdd }
            }
        }, (error, result) => {
            if(error) {
                res.status(error.statusCode ? error.statusCode : 500).send(error.message ? error.message : error);
            } else {
                res.status(200).send(result);
            }
        })
    }
}
