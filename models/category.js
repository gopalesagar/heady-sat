const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const changeCase = require('change-case')

let Category = new Schema(
    {
        name: {
            type: String,
            lowercase: true,
            trim: true,
            index: true,
            unique: true,
            required: true
        },
        displayName: {
            type: String,
            trim: true
        },
        child_categories: [{
            name: String,
            child_categories: [String]
        }],
        products: [{
            type: Schema.Types.ObjectId, ref: 'Product'
        }]
    }
)

Category.pre('save', function (next) {
    this.displayName = changeCase.pascal(this.name);
    next();
});

module.exports = mongoose.model('Category', Category);