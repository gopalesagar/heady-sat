const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');

let Product = new Schema(
    {
        name: {
            type: String,
            lowercase: true,
            trim: true,
            index: true,
            unique: true,
            required: true,
        },
        pricePerUnit: {
            type: Number, 
            trim: true, 
            required: true 
        },
        priceUnit: {
            type: String,
            trim: true,
            required: true
        },
        unit: {
            type: String,
            trim: true,
            required: true
        },
        categories: [{
            type: Schema.Types.ObjectId, 
            ref: 'Category',
            required: true
        }]
    }
)

module.exports = mongoose.model('Product', Product);