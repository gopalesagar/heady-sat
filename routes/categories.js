var express = require('express');
var router = express.Router();
var CategoryController = require('../controllers/categoryController');

router.post('/', CategoryController.create);

router.get('/', CategoryController.list);

module.exports = router;
