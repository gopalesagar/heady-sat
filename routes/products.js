var express = require('express');
var router = express.Router();
var ProductController = require('../controllers/productController');

router.get('/', ProductController.find);

router.post('/', ProductController.create);

router.put('/', ProductController.update);

module.exports = router;
